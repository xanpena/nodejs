# Node Essentials

- https://nodejs.org/dist/latest-v20.x/docs/api/
- https://nodemon.io/
  - npm install -g nodemon
- https://pm2.keymetrics.io/

- npm i -g node-gyp // Native modules like C++

- https://www.toptal.com/developers/gitignore/

- npm i @faker-js/faker

- https://insomnia.rest/download
- https://www.postman.com/downloads/

- https://www.npmjs.com/package/@hapi/boom
- https://joi.dev/

  - https://www.npmjs.com/package/joi

- http://expressjs.com/en/resources/middleware/cors.html
  - https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
- http://expressjs.com/en/resources/middleware/morgan.html
- https://github.com/helmetjs/helmet
- https://github.com/devoidfury/express-debug
- https://github.com/ericf/express-slash
- https://github.com/jaredhanson/passport
- http://expressjs.com/en/resources/middleware.html

- https://node-postgres.com/

- npm i dotenv

- https://sequelize.org/
- https://www.passportjs.org/
- https://jwt.io/
- https://keygen.io/
- https://ethereal.email/
