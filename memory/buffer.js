// let buffer = Buffer.alloc(4)
// let buffer = Buffer.from([1, 2, 3])
let buffer = Buffer.from('Hola')

console.log(buffer)
console.log(buffer.toString())

let abc = Buffer.alloc(26)
console.log(abc)

for (let counter = 0; counter < 26; counter++) {
    abc[counter] = counter + 97
}

console.log(abc.toString())