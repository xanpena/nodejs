const fs = require('fs')
const stream = require('stream')
const util = require('util')

let data = ''

let readableStream = fs.createReadStream(__dirname + '/_file.txt')

readableStream.setEncoding('UTF8')
readableStream.on('data', function(chunk) {
    console.log(chunk.toString())
    data += chunk
})

readableStream.on('end', () => console.log(data))


process.stdout.write('Hello')
process.stdout.write('Node')

const Transform = stream.Transform

function upperText() {
    Transform.call(this)
}

util.inherits(upperText, Transform)

upperText.prototype._transform = function (chunk, codif, cb) {
    chunkUpper = chunk.toString().toUpperCase()
    this.push(chunkUpper)
    cb()
}

let upper = new upperText()

readableStream
    .pipe(upper)
    .pipe(process.stdout)
