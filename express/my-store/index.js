const express = require('express')
const routerApi = require('./routes')
const cors = require('cors')
const passport = require('passport')

const { logErrors, errorHandler, boomErrorHandler, ormErrorHandler} = require('./middlewares/error.handler')
const { checkApiKey } = require('./middlewares/auth.handler')

const app = express()
const port = process.env.PORT || 3000

app.use(passport.initialize());
require('./utils/auth')

app.use(express.json())

app.get('/', (req, res) => {
    res.send('Hello from Express')
})

routerApi(app)

// Multiple params
app.get('/categories/:categoryId/products/:productId',
  checkApiKey,
  (req, res) => {
    const { categoryId, productId } = req.params;
    res.json({
        categoryId,
        productId
    })
  }
)

// Optional params
app.get('/users', (req, res) => {
    const { limit, offset } = req.query;
    if (limit && offset) {
        res.json({
            limit,
            offset
        });
    }
    res.send('Not parameters')
})

app.listen(port, () => {
    console.log('listening...')
})

// Middlewares ever after router
const whitelist = [
    'http://localhost:8080',
    'https://myapp.example'
]
const options = {
    origin: (origin, callback) => {
        if (whitelist.includes(origin) || !origin) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed'))
        }
    }
}
app.use(cors(options))
app.use(logErrors)
app.use(ormErrorHandler);
app.use(boomErrorHandler)
app.use(errorHandler)
