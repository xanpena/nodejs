CREATE TABLE products (
    id serial PRIMARY KEY,
    name VARCHAR (255) NOT NULL,
    actived boolean DEFAULT false
);