const fs = require('fs')

function read(path, callback) {
    fs.readFile(path, (error, data) => {
        console.log(data.toString())
    })
}

read(__dirname + '/_file.txt')

function write(path, content, callback) {
    fs.writeFile(path, content, (error) => {
        (error) ? console.log('Error ', error) : console.log('Done!')
    })
}
write(__dirname + '/_file2.txt', 'New line')

function trush(path, callback) {
    fs.unlink(path, (error) => {
        (error) ? console.log('Error ', error) : console.log('Done!')
    })
}

trush(__dirname + '/_file2.txt')