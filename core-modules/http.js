const http = require('http')

http.createServer(function(request, response) {
    console.log('New request')
    console.log(request.url)

    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.write('Hello')

    response.end()
}).listen(3000)

