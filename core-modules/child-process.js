const { exec, spawn } = require('child_process')

/*exec('ls -la', (error, stdout, sterror) => {
    if (error) {
        console.error(error)
        return false;
    }
    console.log(stdout)
})*/

exec('dir', (error, stdout, sterror) => {
    if (error) {
        console.error(error)
        return false;
    }
    console.log(stdout)
})

let newProcess = spawn('dir')
console.log(newProcess)
console.log(newProcess.pid)
console.log(newProcess.connected)

newProcess.stdout.on('data', (dato) => {
    console.log(newProcess.killed)
    console.log(dato.toString())
})

newProcess.on('exit', () => {
    console.log('Process ended.')
    console.log(newProcess.killed)
})