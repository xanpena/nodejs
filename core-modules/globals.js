let counter = 0
let interval = setInterval(() => {
    if(counter === 3) {
        clearInterval(interval)
    }
    counter++;
    console.log(global)
}, 1000)

setImmediate(() => {
    console.log('Hello')
})

// require()

console.log(process)
console.log(__dirname)
console.log(__filename)
