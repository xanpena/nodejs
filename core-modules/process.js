// const process = require('process')

process.on('beforeExit', () => {
    console.log('Process is going to finish')
})

process.on('exit', () => {
    console.log('Process is finished')
})

//process.on('uncaughtRejection', )
process.on('uncaughtException', (error, origin) => {
    console.log('Uncaught exception')
    console.error(error)
})
// notExists()