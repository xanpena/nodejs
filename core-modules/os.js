const os = require('os')

console.log(os.arch())
console.log(os.platform())
console.log(os.cpus())
console.log(os.cpus().length)
console.log(os.constants)

console.log(os.freemem())

function kb(bytes) {
    return bytes / 1024
}

console.log(kb(os.freemem()))

console.log(os.homedir())
console.log(os.tmpdir())
console.log(os.hostname())
console.log(os.networkInterfaces())