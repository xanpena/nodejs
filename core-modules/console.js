console.log('Log message')
console.info('Info message')
console.error('Error message')
console.warn('Warning message')

let table = [
    {
        a: 1,
        b: 'Z'
    },
    {
        a: 2,
        b:'X'
    }
]
console.log(table)
console.table(table)

console.group('days')
console.log('Monday')
console.log('Tuesday')
console.log('Saturday')
console.groupEnd('days')

console.count('times');
console.count('times');
console.count('times');
console.countReset('times');
console.count('times');